Source: xcb-util-renderutil
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Uploaders: Arnaud Fontaine <arnau@debian.org>
Section: libdevel
Priority: optional
Build-Depends: debhelper-compat (= 13),
 libxcb1-dev,
 libxcb-render0-dev,
 pkgconf,
 xutils-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/xorg-team/lib/xcb-util-renderutil
Vcs-Git: https://salsa.debian.org/xorg-team/lib/xcb-util-renderutil.git
Homepage: https://xcb.freedesktop.org

Package: libxcb-render-util0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: utility libraries for X C Binding -- render-util
 This package contains the library files needed to run software using
 libxcb-render-util, providing convenience functions for the Render extension.
 .
 The xcb-util module provides a number of libraries which sit on top of
 libxcb, the core X protocol library, and some of the extension
 libraries. These experimental libraries provide convenience functions
 and interfaces which make the raw X protocol more usable. Some of the
 libraries also provide client-side code which is not strictly part of
 the X protocol but which have traditionally been provided by Xlib.

Package: libxcb-render-util0-dev
Architecture: any
Multi-Arch: same
Depends: libxcb-render-util0 (= ${binary:Version}),
 libxcb1-dev,
 libxcb-render0-dev,
 ${misc:Depends}
Description: utility libraries for X C Binding -- render-util (devel)
 This package contains the header and library files needed to build software
 using libxcb-render-util, providing convenience functions for the Render
 extension.
 .
 The xcb-util module provides a number of libraries which sit on top of
 libxcb, the core X protocol library, and some of the extension
 libraries. These experimental libraries provide convenience functions
 and interfaces which make the raw X protocol more usable. Some of the
 libraries also provide client-side code which is not strictly part of
 the X protocol but which have traditionally been provided by Xlib.
